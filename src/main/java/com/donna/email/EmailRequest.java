package com.donna.email;

import com.donna.commontype.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(indexes = {@Index(name = "ix_realized", columnList = "realized")})
public class EmailRequest extends BaseEntity {

    private int errorCount;
    private boolean realized;
    @Column(length = 3000)
    private String encounteredError;
    private String email;
    private String subject;
    @Column(length = 200000)
    private String emailText;
    private long sendingTimestamp;

    public EmailRequest(String email, String subject, String emailText) {
        this.email = email;
        this.subject = subject;
        this.emailText = emailText;
    }

    public void emailSent() {
        realized = true;
        sendingTimestamp = new Date().getTime();
    }

    public void errorEncountered(String message) {
        errorCount++;
        encounteredError = message;
        if (errorCount > 20) {
            realized = true;
        }
    }

}
