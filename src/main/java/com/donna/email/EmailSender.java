package com.donna.email;

public interface EmailSender {

    void sendInviteEmail(String email, Long companyId);

    void sendPasswordEmail(String email);
}

