package com.donna.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@EnableScheduling
public class ReservationMaintenanceService {

    @Autowired
    private ReservationService reservationService;

    @Scheduled(fixedDelay = 5000)
    public void completeReservations() {
        reservationService.reservationScheduler();
    }
}
