package com.donna.service;


import com.donna.dao.EmailRequestRepository;
import com.donna.email.EmailRequest;
import com.donna.email.EmailSender;
import com.donna.email.EmailTemplate;
import com.donna.security.jwt.JwtProvider;
import io.micrometer.core.instrument.util.IOUtils;
import lombok.RequiredArgsConstructor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.List;

@Service
@Transactional
@EnableScheduling
@RequiredArgsConstructor
public class EmailService implements EmailSender {

    private final JavaMailSender emailSender;
    private final EmailRequestRepository emailRequestRepository;
    private final JwtProvider jwtProvider;
    private final Logger logger = LoggerFactory.getLogger(EmailService.class);
    @Value(value = "${mail.from}")
    private String mailFrom;
    @Value(value = "${system.url}")
    private String baseUrl;


    public void emailScheduler() {
        List<EmailRequest> requests = emailRequestRepository.findByRealized(false);
        for (EmailRequest request : requests) {
            try {
                sendEmail(request);
                request.emailSent();
            } catch (RuntimeException e) {
                request.errorEncountered(e.getLocalizedMessage());
                logger.warn("Failed to send message to " + request.getEmail(), e);

            }
            emailRequestRepository.save(request);
        }
    }


    private void createEmailRequest(String userEmail, EmailTemplate emailTemplate, Object[] subjectParams, Object[] contentParams) {

        ClassPathResource resourceContent = new ClassPathResource("emailtemplates/" + "en" + "/" + emailTemplate.name().toLowerCase() + ".html");
        ClassPathResource resourceSubject = new ClassPathResource("emailtemplates/" + "en" + "/" + emailTemplate.name().toLowerCase() + "_subject.txt");
        String emailText;
        String subjectText;
        try {
            emailText = IOUtils.toString(resourceContent.getInputStream(), Charset.defaultCharset());
            subjectText = IOUtils.toString(resourceSubject.getInputStream(), Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        EmailRequest emailRequest =
                new EmailRequest(userEmail, digestVelocityTemplate(subjectText, subjectParams), digestVelocityTemplate(emailText, contentParams));
        emailRequestRepository.save(emailRequest);
    }

    private void sendEmail(EmailRequest request) {
        MimeMessagePreparator mailMessage = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setFrom(mailFrom, "Donna");
            message.setTo(request.getEmail());
            message.setSubject(request.getSubject());
            message.setText(request.getEmailText(), true);
        };
        emailSender.send(mailMessage);
    }

    private String digestVelocityTemplate(String templateText, Object[] params) {
        VelocityContext context = new VelocityContext();
        StringWriter w = new StringWriter();
        for (int i = 0; i < params.length; i++) {
            context.put("v" + i, params[i]);
        }
        Velocity.evaluate(context, w, "error in velocity template", templateText);
        return w.toString();
    }

    @Override
    public void sendInviteEmail(String email, Long companyId) {
        createEmailRequest(email, EmailTemplate.INVITE, new String[]{}, new String[]{baseUrl + "auth/onboarding/" + jwtProvider.generateJwtTokenForOnBoard(email, companyId)});
    }

    @Override
    public void sendPasswordEmail(String email) {
        createEmailRequest(email, EmailTemplate.PASSWORD, new String[]{}, new String[]{baseUrl + "auth/new-password/" + jwtProvider.generateJwtTokenForPasswordRecovery(email)});
    }
}
