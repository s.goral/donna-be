package com.donna.service;

import com.donna.DTOs.ReservationDTO;
import com.donna.DTOs.ReservationFormDTO;
import com.donna.DTOs.ReservationsDataDTO;
import com.donna.DTOs.StatisticsDTO;
import com.donna.dao.AppUserDAO;
import com.donna.dao.ClientDAO;
import com.donna.dao.EmployeeDAO;
import com.donna.dao.ReservationDAO;
import com.donna.entity.*;
import com.donna.types.ReservationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@EnableScheduling
public class ReservationService {

    private final ReservationDAO reservationDAO;

    private final EmployeeDAO employeeDAO;

    private final CompanyService companyService;

    private final ClientDAO clientDAO;

    private final AppUserService appUserService;

    private final AppUserDAO appUserDAO;

    public StatisticsDTO getStatisticsForCompany(Company company) {
        List<Reservation> reservations = reservationDAO.findAllByCompany(company);
        Integer newReservations = reservations.stream().
                filter(reservation -> reservation.getStatus() == ReservationStatus.NEW).toArray().length;
        Integer canceledReservations = reservations.stream()
                .filter(reservation -> reservation.getStatus() == ReservationStatus.CANCELED).toArray().length;
        Integer finishedReservations = reservations.stream()
                .filter(reservation -> reservation.getStatus() == ReservationStatus.FINISHED).toArray().length;
        return new StatisticsDTO(reservations.size(), canceledReservations, newReservations, finishedReservations);
    }

    public void reservationScheduler() {
        List<Reservation> reservations = reservationDAO.findAllByStatusAndEndDateBefore(ReservationStatus.NEW, LocalDateTime.now());
        for (Reservation reservation : reservations) {
            reservation.setStatus(ReservationStatus.FINISHED);
        }
    }

    public Reservation getReservation(Long id) {
        return reservationDAO.findById(id).get();
    }

    public Reservation addReservation(ReservationFormDTO reservationFormDTO, Company company) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dtoStartDate = LocalDateTime.parse(reservationFormDTO.getStartDate(), formatter);
        LocalDateTime dtoEndDate = LocalDateTime.parse(reservationFormDTO.getEndDate(), formatter);
        Reservation reservation = new Reservation();
        Employee employee = employeeDAO.findById(reservationFormDTO.getEmployeeId()).get();
        List<Reservation> newReservations = employee.getReservations().stream().filter(r -> r.getStatus().equals(ReservationStatus.NEW)).collect(Collectors.toList());

        checkReservations(newReservations,dtoStartDate, dtoEndDate);

        Client client = clientDAO.findById(reservationFormDTO.getClientId()).get();
        reservation.setCompany(company);
        reservation.setClient(client);
        reservation.setEmployee(employee);
        reservation.setStartDate(dtoStartDate);
        reservation.setEndDate(dtoEndDate);
        reservation.setService(reservationFormDTO.getService());
        reservation.setStatus(ReservationStatus.NEW);
        reservationDAO.save(reservation);
        return reservation;
    }

    static class SortByDate implements Comparator<Reservation> {
        @Override
        public int compare(Reservation a, Reservation b) {
            return a.getStartDate().compareTo(b.getStartDate());
        }
    }

    public LocalDateTime findNextReservation(int timeInMinutes, long id) {
        Company company = appUserService.findCompanyForCurrentUser();
        int openingHour = company.getOpeningHour();
        int closingHour = company.getClosingHour();
        LocalDateTime timeToCompare = reformatTimeToProperOne(LocalDateTime.now(), openingHour, closingHour, timeInMinutes);
        Employee employee = employeeDAO.findById(id).get();

        List<Reservation> newSortedReservations = employee.getReservations().stream().filter(r -> r.getStatus().equals(ReservationStatus.NEW)).
                collect(Collectors.toList());

        Collections.sort(newSortedReservations, new SortByDate());
        for (Reservation reservation : newSortedReservations) {
            LocalDateTime startDate = reservation.getStartDate();
            LocalDateTime endDate = reservation.getEndDate();
            if (timeToCompare.isBefore(startDate) && timeToCompare.plusMinutes(timeInMinutes).isBefore(endDate)) {
                return timeToCompare;
            } else {
                timeToCompare = reformatTimeToProperOne(endDate, openingHour, closingHour, timeInMinutes);
            }
        }
        return timeToCompare;
    }

    private LocalDateTime reformatTimeToProperOne(LocalDateTime timeToCheck, int openingHour, int closingHour, int reservationTimeGap) {
        if (timeToCheck.isAfter(timeToCheck.toLocalDate().atTime(closingHour, 00)) || timeToCheck.plusMinutes(reservationTimeGap).isAfter(timeToCheck.toLocalDate().atTime(closingHour, 00))) {
            return timeToCheck.plusDays(1).withHour(openingHour).withMinute(00);
        } else if (timeToCheck.isBefore(timeToCheck.toLocalDate().atTime(openingHour, 00))) {
            return timeToCheck.withHour(openingHour).withMinute(00);
        }
        return timeToCheck;
    }

    public Reservation updateReservation(ReservationFormDTO reservationFormDTO, long id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        Reservation reservation = reservationDAO.findById(id).get();
        Employee employee = employeeDAO.findById(reservationFormDTO.getEmployeeId()).get();
        List<Reservation> newReservations = employee.getReservations().stream().filter(r -> r.getStatus().equals(ReservationStatus.NEW) && r.getId() != id).collect(Collectors.toList());
        LocalDateTime dtoStartDate = LocalDateTime.parse(reservationFormDTO.getStartDate(), formatter);
        LocalDateTime dtoEndDate = LocalDateTime.parse(reservationFormDTO.getEndDate(), formatter);
        checkReservations(newReservations,dtoStartDate, dtoEndDate);
        Client client = clientDAO.findById(reservationFormDTO.getClientId()).get();
        reservation.setClient(client);
        reservation.setEmployee(employee);
        reservation.setService(reservationFormDTO.getService());
        reservation.setStartDate(LocalDateTime.parse(reservationFormDTO.getStartDate(), formatter));
        reservation.setEndDate(LocalDateTime.parse(reservationFormDTO.getEndDate(), formatter));
        return reservation;
    }

    public void checkReservations(List<Reservation> reservations, LocalDateTime start, LocalDateTime end){
        for (Reservation reservation1 : reservations) {
            LocalDateTime startDate = reservation1.getStartDate();
            LocalDateTime endDate = reservation1.getEndDate();
            if ((start.plusMinutes(1).isAfter(startDate) && end.minusMinutes(1).isBefore(endDate))) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Client is not available");
            }
            if ((start.minusMinutes(1).isBefore(startDate) && end.isAfter(startDate))) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Client is not available");
            }
            if ((start.isBefore(endDate) && end.isAfter(endDate))) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Client is not available");
            }
            if ((start.minusMinutes(1).isBefore(startDate) && end.plusMinutes(1).isAfter(endDate))) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Client is not available");
            }
        }
    }

    public void archiveReservation(Long id) {
        Company company = appUserService.findCompanyForCurrentUser();
        Reservation reservation = getReservation(id);

        if (reservation.getCompany().getId() == company.getId()) {
            Client client = clientDAO.findById(reservation.getClient().getId()).get();
            client.setReservations(client.getReservations().stream().
                    filter(r -> r.getId() != reservation.getId()).collect(Collectors.toList()));
            reservation.setStatus(ReservationStatus.CANCELED);
        }
    }

    public ReservationsDataDTO getReservations(Company company) {
        List<ReservationDTO> reservations = reservationDAO.findAllByCompany(company).stream().map(reservation -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setClientName(reservation.getClient().getName() + " " + reservation.getClient().getLastName());
            reservationDTO.setClientId(reservation.getClient().getId());
            reservationDTO.setEndDate(reservation.getEndDate());
            reservationDTO.setStartDate(reservation.getStartDate());
            reservationDTO.setId(reservation.getId());
            reservationDTO.setService(reservation.getService());
            long ownerId = reservation.getEmployee().getId();
            reservationDTO.setOwnerId(ownerId);
            AppUser appUser = appUserService.findUserByEmployee(employeeDAO.findById(ownerId).get());
            reservationDTO.setOwnerName(appUser.getName() + " " + appUser.getLastName());
            reservationDTO.setStatus(reservation.getStatus());
            return reservationDTO;
        }).collect(Collectors.toList());
        return new ReservationsDataDTO(reservations, company.getOpeningHour(), company.getClosingHour(), company.getServices());
    }

    public List<ReservationDTO> getReservationsForEmployee(long id){
        Employee employee = appUserDAO.findById(id).get().getEmployee();
        return employee.getReservations().stream().map(reservation -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setClientName(reservation.getClient().getName() + " " + reservation.getClient().getLastName());
            reservationDTO.setClientId(reservation.getClient().getId());
            reservationDTO.setEndDate(reservation.getEndDate());
            reservationDTO.setStartDate(reservation.getStartDate());
            reservationDTO.setId(reservation.getId());
            reservationDTO.setService(reservation.getService());
            long ownerId = reservation.getEmployee().getId();
            reservationDTO.setOwnerId(ownerId);
            AppUser appUser = appUserService.findUserByEmployee(employeeDAO.findById(ownerId).get());
            reservationDTO.setOwnerName(appUser.getName() + " " + appUser.getLastName());
            reservationDTO.setStatus(reservation.getStatus());
            return reservationDTO;
        }).collect(Collectors.toList());
    }

    public List<ReservationDTO> getReservationsForClient(long id){
        Client client = clientDAO.findById(id).get();
        return client.getReservations().stream().map(reservation -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setClientName(reservation.getClient().getName() + " " + reservation.getClient().getLastName());
            reservationDTO.setClientId(reservation.getClient().getId());
            reservationDTO.setEndDate(reservation.getEndDate());
            reservationDTO.setStartDate(reservation.getStartDate());
            reservationDTO.setId(reservation.getId());
            reservationDTO.setService(reservation.getService());
            long ownerId = reservation.getEmployee().getId();
            reservationDTO.setOwnerId(ownerId);
            AppUser appUser = appUserService.findUserByEmployee(employeeDAO.findById(ownerId).get());
            reservationDTO.setOwnerName(appUser.getName() + " " + appUser.getLastName());
            reservationDTO.setStatus(reservation.getStatus());
            return reservationDTO;
        }).collect(Collectors.toList());
    }

    @Data
    @AllArgsConstructor
    class DataRangeObject {
        LocalDateTime startDate;
        LocalDateTime endDate;
    }


}
