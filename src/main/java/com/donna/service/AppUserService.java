package com.donna.service;

import com.donna.DTOs.EmployeeDTO;
import com.donna.DTOs.RegisterFormDTO;
import com.donna.DTOs.UserDTO;
import com.donna.DTOs.UserInvitationDTO;
import com.donna.dao.AppUserDAO;
import com.donna.dao.EmployeeDAO;
import com.donna.dao.ReservationDAO;
import com.donna.entity.AppUser;
import com.donna.entity.Company;
import com.donna.entity.Employee;
import com.donna.utils.RoleName;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class AppUserService implements UserDetailsService {

    private final AppUserDAO appUserDAO;

    private final EmailService emailService;

    private final PasswordEncoder encoder;

    private final ReservationDAO reservationDAO;

    private final EmployeeDAO employeeDAO;


    public AppUser findUserByUsername(String username) {
        if(appUserDAO.findByUsername(username).isPresent())
        return appUserDAO.findByUsername(username).get();

        return null;
    }

    public List<UserDTO> getAllUsers() {
        return appUserDAO.findAll().stream().filter(user -> !user.getRole().equals(RoleName.ROLE_ADMIN)).map(user ->
                new UserDTO(user.getId(), user.getName(), user.getLastName(), user.getUsername(), user.getRole(), user.isActive())
        ).collect(Collectors.toList());
    }

    public UserDetails loadUserByUsername(String username) {
        Optional<AppUser> userOptional = appUserDAO.findByUsername(username.toLowerCase());

        if (!userOptional.isPresent()) {
            throw new UsernameNotFoundException("Invalid username or password.");
        } else {
            AppUser user = userOptional.get();
            return new User(user.getUsername(), user.getPassword(),
                    Collections.singletonList(new SimpleGrantedAuthority(user.getRole().name())));
        }
    }

    public void updateRoleName(RoleName roleName, long id){
        appUserDAO.findById(id).get().setRole(roleName);
    }

    public Company findCompanyForCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        AppUser user = findUserByUsername(auth.getName());

        return user.getCompany();
    }

    public AppUser findCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        AppUser user = findUserByUsername(auth.getName());

        return user;
    }

    public void deleteUser(long id){
        AppUser appUser = appUserDAO.findById(id).get();
        Employee employee = appUser.getEmployee();
        reservationDAO.findAll().forEach(reservation -> {
            if (reservation.getEmployee().getId() == employee.getId()) {
                reservation.setEmployee(null);
                reservation.setCompany(null);
                reservation.setClient(null);
                reservationDAO.delete(reservation);
            }
        });
        appUser.setCompany(null);
        employeeDAO.delete(employee);
        appUserDAO.deleteById(id);
    }

    public void addUsersFromInvitation(List<UserInvitationDTO> invitationDTOList) {
        Company company = findCompanyForCurrentUser();
        invitationDTOList.forEach(invitationDTO -> {
            AppUser appUser = new AppUser();
            appUser.setUsername(invitationDTO.getEmail());
            appUser.setActive(false);
            appUser.setCompany(company);
            appUser.setRole(invitationDTO.getRole());
            if (appUserDAO.findByUsername(invitationDTO.getEmail()).isEmpty()) {
                appUserDAO.save(appUser);
                emailService.sendInviteEmail(invitationDTO.getEmail(), company.getId());
            }
        });
    }


    public AppUser findUserByEmployee(Employee employee) {
        return appUserDAO.findByEmployee(employee).get();
    }

    public AppUser createNewAppUser(RegisterFormDTO registerFormDTO, RoleName roleName, Company company) {
        AppUser appUser = AppUser.createUser(registerFormDTO, roleName, encoder, company, null);
        return appUserDAO.save(appUser);
    }
}
