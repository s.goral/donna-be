package com.donna.service;

import com.donna.DTOs.ClientDTO;
import com.donna.DTOs.ClientFormDTO;
import com.donna.dao.ClientDAO;
import com.donna.dao.ReservationDAO;
import com.donna.entity.Client;
import com.donna.entity.Company;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Transactional
public class ClientService {

    private final ClientDAO clientDAO;

    private final AppUserService appUserService;

    private final ReservationDAO reservationDAO;

    public List<ClientDTO> getAllClients() {
        return clientDAO.findAll().stream().map(client ->
                new ClientDTO(client.getId(), client.getName(), client.getLastName(), client.getPhoneNumber(), client.getEmail(), client.getJoinDate(), client.getBirthday(), client.getGender())
        ).collect(Collectors.toList());
    }


    public ClientDTO getClientById(long id) {
        Client client = clientDAO.findById(id).get();

        return new ClientDTO
                (client.getId(), client.getName(), client.getLastName(), client.getPhoneNumber(),
                        client.getEmail(), client.getJoinDate(), client.getBirthday(), client.getGender());
    }

    public void deleteClient(long id) {
        Client client = clientDAO.findById(id).get();
        reservationDAO.findAll().forEach(reservation -> {
            if (reservation.getClient().getId() == id) {
                reservation.setEmployee(null);
                reservation.setCompany(null);
                reservation.setClient(null);
                reservationDAO.delete(reservation);
            }
        });
        client.setCompany(null);
        clientDAO.deleteById(id);
    }

    public void editClient(ClientFormDTO clientFormDTO, long id) {
        Client client = clientDAO.findById(id).get();
        client.setJoinDate(LocalDate.now());
        client.setGender(clientFormDTO.getGender());
        client.setBirthday(clientFormDTO.getBirthday());
        client.setName(clientFormDTO.getName());
        client.setLastName(clientFormDTO.getLastName());
        client.setEmail(clientFormDTO.getEmail());
        client.setPhoneNumber(clientFormDTO.getPhoneNumber());
    }

    public Client addClient(ClientFormDTO clientFormDTO, Company company) {
        Client client = new Client();
        client.setJoinDate(LocalDate.now());
        client.setGender(clientFormDTO.getGender());
        client.setBirthday(clientFormDTO.getBirthday());
        client.setName(clientFormDTO.getName());
        client.setLastName(clientFormDTO.getLastName());
        client.setEmail(clientFormDTO.getEmail());
        client.setPhoneNumber(clientFormDTO.getPhoneNumber());
        client.setCompany(company);
        return clientDAO.save(client);
    }

}
