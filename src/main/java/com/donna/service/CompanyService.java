package com.donna.service;

import com.donna.DTOs.CompanyDTO;
import com.donna.DTOs.CompanyDataDTO;
import com.donna.DTOs.RegisterFormDTO;
import com.donna.dao.CompanyDAO;
import com.donna.entity.Client;
import com.donna.entity.Company;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class CompanyService {

    private final CompanyDAO companyDAO;

    private final PasswordEncoder encoder;

    private final AppUserService appUserService;

    private final ClientService clientService;

    public Company getCompanyById(Long id) {
        return companyDAO.findById(id).get();
    }

    public List<CompanyDTO> getAllCompanies() {
        return companyDAO.findAll().stream().
                map(company ->
                        new CompanyDTO(company.getId(),
                                company.getName(), company.getClients().size(), company.getEmployees().size())
                ).
                collect(Collectors.toList());
    }

    public void delete(long id){
        Company company = companyDAO.findById(id).get();
        company.getClients().forEach(client -> {
            clientService.deleteClient(client.getId());
        });
        company.getEmployees().forEach(user -> {
            appUserService.deleteUser(user.getId());
        });
        companyDAO.delete(company);
    }

    public CompanyDataDTO getData(){
        Company company = appUserService.findCompanyForCurrentUser();
        return new CompanyDataDTO(company.getName(), company.getOpeningHour(), company.getClosingHour(), company.getServices());
    }

    public void update(CompanyDataDTO companyDataDTO){
        Company company = appUserService.findCompanyForCurrentUser();
        company.setClosingHour(companyDataDTO.getClosingHour());
        company.setOpeningHour(companyDataDTO.getOpeningHour());
        company.setName(companyDataDTO.getName());
        company.setServices(companyDataDTO.getServices().stream().filter(v -> v != null && !v.equals("")).collect(Collectors.toList()));
    }

    public Company addCompany(RegisterFormDTO registerFormDTO) {
        Company company = Company.createNewCompany(registerFormDTO);
        return companyDAO.save(company);
    }
}
