package com.donna.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@EnableScheduling
public class EmailMaintenanceService {

    @Autowired
    private EmailService emailService;

    @Scheduled(fixedDelay = 5000)
    public void sendRequestedMails() {
        emailService.emailScheduler();
    }
}
