package com.donna;


import com.donna.DTOs.RegisterFormDTO;
import com.donna.dao.*;
import com.donna.entity.*;
import com.donna.security.jwt.JwtProvider;
import com.donna.service.EmailService;
import com.donna.types.ReservationStatus;
import com.donna.utils.Gender;
import com.donna.utils.RoleName;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

@Component
@Transactional
@RequiredArgsConstructor
public class Startup implements ApplicationRunner {
    private final JwtProvider jwtProvider;
    private final AppUserDAO appUserDAO;
    private final CompanyDAO companyDAO;
    private final ReservationDAO reservationDAO;
    private final ClientDAO clientDAO;
    private final EmployeeDAO employeeDAO;
    private final EmailService emailService;
    private final PasswordEncoder bCryptPasswordEncoder;
    @Value("${populatedb}")
    private Boolean populateDB;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (BooleanUtils.isTrue(populateDB)) {
            populateDb();
        }
    }

    private void populateDb() {
        addAppUsers();
    }

    private void addAppUsers() {
        RegisterFormDTO registerFormDTO = new RegisterFormDTO("sebastian.goral37@gmail.com", "asdasdasd", "Company", "Seba", "Goral", 7, 20,Arrays.asList("A", "B", "C", "D"), null);
        RegisterFormDTO userRegisterFormDTO = new RegisterFormDTO("sebastian.goral37+user@gmail.com", "asdasdasd", "null", "Seba", "Goral", 7, 20, null, null);
        RegisterFormDTO adminRegisterFormDTO = new RegisterFormDTO("admin", "asdasdasd", "null", "Seba", "Goral", 7, 20, null, null);

        Company company = companyDAO.save(Company.createNewCompany(registerFormDTO));
        Employee employee = employeeDAO.save(new Employee());
        Employee employee2 = employeeDAO.save(new Employee());

        company.setClients(new ArrayList<>());
        for (int i = 0; i <20 ; i++) {
            Client client = clientDAO.save(new Client("Client", "clientowski",
                    "seba@asd.com", "123123123", LocalDate.now().withMonth((int)(Math.random()*12+1)), LocalDate.now().withMonth((int)(Math.random()*12+1)), Gender.MALE, company, null));
            company.getClients().add(client);
            int start = (int)(Math.random()*4) + 7;
            int end = (int)(Math.random()*4) + start;
            int day = (int)(Math.random()*27+1);
            int month =(int)(Math.random()*11+1);
            if(i%2 == 0) {
                reservationDAO.save(new Reservation(ReservationStatus.NEW, LocalDate.now().plusMonths(1).withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().plusMonths(1).withDayOfMonth(day).atTime(end, 45), company, client, employee, "A"));
                reservationDAO.save(new Reservation(ReservationStatus.FINISHED, LocalDate.now().withMonth(month).withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().withMonth(month).withDayOfMonth(day).atTime(end, 45), company, client, employee, "A"));
                reservationDAO.save(new Reservation(ReservationStatus.CANCELED, LocalDate.now().withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().withMonth(month).withDayOfMonth(day).atTime(end, 45), company, client, employee, "A"));
            } else {
                reservationDAO.save(new Reservation(ReservationStatus.NEW, LocalDate.now().plusMonths(1).withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().plusMonths(1).withDayOfMonth(day).atTime(end, 45), company, client, employee2, "A"));
                reservationDAO.save(new Reservation(ReservationStatus.FINISHED, LocalDate.now().withMonth(month).withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().withMonth(month).withDayOfMonth(day).atTime(end, 45), company, client, employee2, "A"));
                reservationDAO.save(new Reservation(ReservationStatus.CANCELED, LocalDate.now().withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().withDayOfMonth(day).atTime(end, 45), company, client, employee2, "A"));
            }
            if(i%5 == 0){
                if(i%2 == 0) {
                    reservationDAO.save(new Reservation(ReservationStatus.NEW, LocalDate.now().atTime(start, 00),
                            LocalDate.now().atTime(end, 45), company, client, employee, "A"));
                } else {
                    reservationDAO.save(new Reservation(ReservationStatus.NEW, LocalDate.now().atTime(start, 00),
                            LocalDate.now().atTime(end, 45), company, client, employee2, "A"));
                }
                reservationDAO.save(new Reservation(ReservationStatus.NEW, LocalDate.now().minusYears(1).withDayOfMonth(day).atTime(start, 00),
                        LocalDate.now().minusYears(1).plusMonths(1).withDayOfMonth(day).atTime(end, 45), company, client, employee, "A"));
            }
        }

        AppUser adminUser = AppUser.createUser(registerFormDTO, RoleName.ROLE_USER_ADMIN, bCryptPasswordEncoder, company, employee2);
        AppUser user = AppUser.createUser(userRegisterFormDTO, RoleName.ROLE_USER, bCryptPasswordEncoder, company, employee);
        AppUser admin = AppUser.createUser(adminRegisterFormDTO, RoleName.ROLE_ADMIN, bCryptPasswordEncoder, null, null);


        appUserDAO.save(adminUser);
        appUserDAO.save(user);
        appUserDAO.save(admin);
    }


}
