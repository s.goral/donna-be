package com.donna.utils;

public class RequiredEntityNotFoundException extends RuntimeException {

    public RequiredEntityNotFoundException(String message) {
        super(message);
    }

}
