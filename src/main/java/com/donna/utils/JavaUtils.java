package com.donna.utils;/*
package com.tin.java.utils;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class JavaUtils {

    private JavaUtils() {
    }

    private static final String ENTITY_NOT_FOUND_EXCEPTION = "Entity not found exception!";

    public static <T> void ifPresentOrException(Optional<T> optional, Consumer<? super T> action) throws RequiredEntityNotFoundException {
        optional.ifPresentOrElse(action, () -> {
            throw new RequiredEntityNotFoundException(ENTITY_NOT_FOUND_EXCEPTION);
        });
    }

    public static <T> void ifPresentOrInvalidRequest(Optional<T> optional, Consumer<? super T> action) throws InvalidRequestException {
        optional.ifPresentOrElse(action, () -> {
            throw new InvalidRequestException("Invalid request exception!");
        });
    }

    public static <T, R> R ifPresentOrExceptionWithReturn(Optional<T> optional, Function<T, R> function) throws RequiredEntityNotFoundException {
        if (optional.isPresent()) {
            return function.apply(optional.get());
        } else {
            throw new RequiredEntityNotFoundException(ENTITY_NOT_FOUND_EXCEPTION);
        }
    }

    public static <R> R returnOrException(Optional<R> optional) throws RequiredEntityNotFoundException {
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new RequiredEntityNotFoundException(ENTITY_NOT_FOUND_EXCEPTION);
        }
    }

    public static <R> R returnOrInvalidRequestException(Optional<R> optional) throws InvalidRequestException {
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new InvalidRequestException("Invalid request exception!");
        }
    }

}*/
