package com.donna.utils;

public enum RoleName {
    ROLE_ADMIN, ROLE_USER, ROLE_USER_ADMIN
}
