package com.donna.rest;

import com.donna.DTOs.*;
import com.donna.dao.AppUserDAO;
import com.donna.entity.AppUser;
import com.donna.entity.Client;
import com.donna.entity.Company;
import com.donna.entity.Employee;
import com.donna.service.AppUserService;
import com.donna.service.ClientService;
import com.donna.service.CompanyService;
import com.donna.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/client-company")
public class CompanyController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    CompanyService companyService;

    @Autowired
    ReservationService reservationService;

    @Autowired
    ClientService clientService;

    @Autowired
    AppUserDAO appUserDAO;

    @GetMapping(value = "/statistics")
    public StatisticsDTO getStatisticsForCompany() {
        return reservationService.getStatisticsForCompany(appUserService.findCompanyForCurrentUser());
    }

    @GetMapping(value = "/clients")
    public List<ClientDTO> getClients() {
        return appUserService.findCompanyForCurrentUser().getClients().stream().map(client ->
                new ClientDTO(client.getId(), client.getName(), client.getLastName(), client.getPhoneNumber(), client.getEmail(), client.getJoinDate(), client.getBirthday(), client.getGender())
        ).collect(Collectors.toList());
    }


    @GetMapping(value = "/employees")
    public List<EmployeeDTO> getEmployees() {
        return appUserService.findCompanyForCurrentUser().getEmployees().stream().filter(user -> user.getEmployee() != null).map(user ->
                new EmployeeDTO(user.getEmployee().getId(), user.getName(), user.getLastName(), user.getUsername())
        ).collect(Collectors.toList());
    }

    @PostMapping(value = "/next-reservation/user/{id}")
    public LocalDateTime findNextReservation(@RequestBody int timeInMinutes, @PathVariable long id) {
       return reservationService.findNextReservation(timeInMinutes,id);
    }

    @GetMapping(value = "/employee/data/{id}")
    public PersonDataDTO getEmployeesReservations(@PathVariable long id){
        PersonDataDTO data = new PersonDataDTO();
        AppUser appUser = appUserDAO.findById(id).get();
        data.setReservations(reservationService.getReservationsForEmployee(id));
        data.setName(appUser.getName());
        data.setLastName(appUser.getLastName());
        return data;
    }

    @GetMapping(value = "/client/data/{id}")
    public PersonDataDTO getClientsReservations(@PathVariable long id){
        PersonDataDTO data = new PersonDataDTO();
        ClientDTO client = clientService.getClientById(id);
        data.setReservations(reservationService.getReservationsForClient(id));
        data.setName(client.getName());
        data.setLastName(client.getLastName());
        return data;
    }

    @GetMapping(value = "/users")
    public List<UserDTO> getUsers() {
        return appUserService.findCompanyForCurrentUser().getEmployees().stream().map(user ->
                new UserDTO(user.getId(), user.getName(), user.getLastName(), user.getUsername(), user.getRole(), user.isActive())
        ).collect(Collectors.toList());
    }

    @GetMapping(value = "/reservations")
    public ReservationsDataDTO getReservations() {
        Company company = appUserService.findCompanyForCurrentUser();
        return reservationService.getReservations(company);
    }

    @PostMapping(value = "/archive-reservation/{id}")
    public void archiveReservation(@PathVariable Long id) {
        reservationService.archiveReservation(id);
    }

    @PostMapping(value = "/add-client")
    public void addClient(@RequestBody ClientFormDTO clientFormDTO) {
        Company company = appUserService.findCompanyForCurrentUser();
        clientService.addClient(clientFormDTO, company);
    }

    @PutMapping(value = "/edit-client/{id}")
    public void updateClient(@RequestBody ClientFormDTO clientFormDTO, @PathVariable Long id) {
        clientService.editClient(clientFormDTO, id);
    }

    @GetMapping(value = "/client/{id}")
    public ClientDTO getClient(@PathVariable Long id) {
        return clientService.getClientById(id);
    }

    @GetMapping(value = "/company")
    public CompanyDataDTO getCompanyData() {
        return companyService.getData();
    }

    @PutMapping(value = "/edit-company")
    public void updateClient(@RequestBody CompanyDataDTO companyDataDTO) {
        companyService.update(companyDataDTO);
    }

    @DeleteMapping(value = "/client/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.deleteClient(id);
    }

    @DeleteMapping(value = "/delete/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        appUserService.deleteUser(id);
    }

    @PostMapping(value = "/add-reservation")
    public void addReservation(@RequestBody ReservationFormDTO reservationFormDTO) throws Exception {
        Company company = appUserService.findCompanyForCurrentUser();
        reservationService.addReservation(reservationFormDTO, company);
    }

    @PostMapping(value = "invite-users")
    public void inviteUsers(@RequestBody List<UserInvitationDTO> invitationDTOS) {
        appUserService.addUsersFromInvitation(invitationDTOS);
    }

    @PutMapping(value = "/edit-reservation/{id}")
    public void editReservation(@RequestBody ReservationFormDTO reservationFormDTO, @PathVariable Long id) {
        reservationService.updateReservation(reservationFormDTO, id);
    }


}
