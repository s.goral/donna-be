package com.donna.rest;

import com.donna.DTOs.*;
import com.donna.dao.AppUserDAO;
import com.donna.dao.EmployeeDAO;
import com.donna.entity.AppUser;
import com.donna.entity.Company;
import com.donna.entity.Employee;
import com.donna.security.jwt.JwtProvider;
import com.donna.service.AppUserService;
import com.donna.service.CompanyService;
import com.donna.service.EmailService;
import com.donna.utils.RoleName;
import com.sun.tools.jconsole.JConsoleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AppUserDAO userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    EmailService emailService;

    @Autowired
    CompanyService companyService;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    AppUserService appUserService;

    @Autowired
    AppUserDAO appUserDAO;


    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginFormDTO loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    @PostMapping("/forgot")
    public void forgotPassword(@RequestBody String email){
        AppUser appUser = appUserService.findUserByUsername(email);
        if(appUser != null){
            emailService.sendPasswordEmail(email);
        }
    }

    @PutMapping("/change-password")
    public void changePassword(@RequestBody ChangePasswordDTO changePasswordDTO){
        String userName = jwtProvider.getUserNameFromJwtToken(changePasswordDTO.getToken());
        AppUser appUser = appUserService.findUserByUsername(userName);
        if(appUser != null){
            appUser.setPassword(encoder.encode(changePasswordDTO.getNewPassword()));
            appUserDAO.save(appUser);
        }
    }

    @GetMapping("/user-details")
    public UserDTO getUserDetails() {
        AppUser user = appUserService.findCurrentUser();
        if(user != null){
            return new UserDTO(user.getId(), user.getName(), user.getLastName(), user.getUsername(), user.getRole(), user.isActive());
        }
        return null;
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterFormDTO signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        Employee employee = employeeDAO.save(new Employee(new ArrayList<>()));
        Company company = companyService.addCompany(signUpRequest);
        AppUser user = AppUser.createUser(signUpRequest, RoleName.ROLE_USER_ADMIN, encoder, company, employee);

        userRepository.save(user);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }

    @PostMapping("/onboard")
    public ResponseEntity<?> onboardUser(@Valid @RequestBody OnboardFormDTO signUpRequest) {
        AppUser user = appUserService.findUserByUsername(signUpRequest.getUsername());
        Employee employee = new Employee(new ArrayList<>());
        user.setActive(true);
        user.setName(signUpRequest.getName());
        user.setName(signUpRequest.getName());
        user.setLastName(signUpRequest.getLastName());
        user.setPassword(encoder.encode(signUpRequest.getPassword()));
        user.setEmployee(employee);
        userRepository.save(user);
        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }


}
