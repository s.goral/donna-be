package com.donna.rest;

import com.donna.DTOs.*;
import com.donna.service.AppUserService;
import com.donna.service.ClientService;
import com.donna.service.CompanyService;
import com.donna.service.ReservationService;
import com.donna.utils.RoleName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    CompanyService companyService;

    @Autowired
    ReservationService reservationService;

    @Autowired
    ClientService clientService;

    @GetMapping(value = "/clients")
    public List<ClientDTO> getClients() {
        return clientService.getAllClients();
    }

    @PutMapping(value = "/edit-client/{id}")
    public void updateClient(@RequestBody ClientFormDTO clientFormDTO, @PathVariable Long id) {
        clientService.editClient(clientFormDTO, id);
    }

    @PatchMapping(value = "/user/role/{id}")
    public void updateUserRoleName(@RequestBody RoleName roleName, @PathVariable Long id){
        appUserService.updateRoleName(roleName,id);
    }

    @DeleteMapping(value = "/delete/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        appUserService.deleteUser(id);
    }

    @DeleteMapping(value = "/delete/company/{id}")
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }


    @DeleteMapping(value = "/client/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.deleteClient(id);
    }

    @GetMapping(value = "/employees")
    public List<UserDTO> getEmployees() {
        return appUserService.getAllUsers();
    }

    @GetMapping(value = "/client/{id}")
    public ClientDTO getClient(@PathVariable Long id) {
        return clientService.getClientById(id);
    }

    @GetMapping(value = "/companies")
    public List<CompanyDTO> getCompanies() {
        return companyService.getAllCompanies();
    }

}
