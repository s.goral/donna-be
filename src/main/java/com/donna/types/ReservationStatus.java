package com.donna.types;

public enum ReservationStatus {
    CANCELED, NEW, FINISHED
}
