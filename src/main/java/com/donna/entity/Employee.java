package com.donna.entity;

import com.donna.commontype.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "employee")
public class Employee extends BaseEntity {

    @OneToMany(mappedBy = "employee")
    List<Reservation> reservations;
}
