package com.donna.entity;

import com.donna.commontype.BaseEntity;
import com.donna.types.ReservationStatus;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "reservation")
public class Reservation extends BaseEntity {

    private ReservationStatus status;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "fk_company"))
    private Company company;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "client_id", foreignKey = @ForeignKey(name = "fk_client"))
    private Client client;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "fk_employee"))
    private Employee employee;

    private String service;
}
