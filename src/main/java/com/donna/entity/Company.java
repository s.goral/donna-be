package com.donna.entity;

import com.donna.DTOs.RegisterFormDTO;
import com.donna.commontype.BaseEntity;
import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "company")
public class Company extends BaseEntity {
    @OneToMany(mappedBy = "company")
    List<Client> clients;
    @OneToMany(mappedBy = "company")
    List<AppUser> employees;
    private String name;

    private int openingHour;
    private int closingHour;

    @ElementCollection
    private List<String> services;

    public static Company createNewCompany(RegisterFormDTO companyDTO) {
        Company company = new Company();
        company.setName(companyDTO.getCompanyName());
        company.setOpeningHour(companyDTO.getOpeningHour());
        company.setClosingHour(companyDTO.getClosingHour());
        company.setServices(companyDTO.getServices());
        return company;
    }
}
