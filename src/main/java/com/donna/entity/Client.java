package com.donna.entity;

import com.donna.commontype.BaseEntity;
import com.donna.utils.Gender;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "client")
public class Client extends BaseEntity {
    private String name;
    private String lastName;
    private String email;
    private String phoneNumber;
    private LocalDate joinDate;
    private LocalDate birthday;
    private Gender gender;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "fk_company"))
    private Company company;

    @OneToMany(mappedBy = "client")
    List<Reservation> reservations;
}
