package com.donna.entity;

import com.donna.DTOs.RegisterFormDTO;
import com.donna.commontype.BaseEntity;
import com.donna.utils.RoleName;
import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_user", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        })
})
public class AppUser extends BaseEntity {

    @Size(max = 256)
    String name;
    @Size(max = 256)
    String lastName;
    @Column(unique = true)
    @Size(min = 5, max = 254)
    private String username;
    @NotBlank
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private RoleName role;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "fk_company"))
    private Company company;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "fk_employee"))
    private Employee employee;

    private boolean isActive;

    public static AppUser createUser(RegisterFormDTO registerFormDTO, RoleName role, PasswordEncoder passwordEncoder, Company company, Employee employee) {
        AppUser appUser = new AppUser();
        appUser.setRole(role);
        appUser.setUsername(registerFormDTO.getUsername());
        appUser.setPassword(passwordEncoder.encode(registerFormDTO.getPassword()));
        appUser.setCompany(company);
        appUser.setName(registerFormDTO.getName());
        appUser.setLastName(registerFormDTO.getLastName());
        appUser.setEmployee(employee);
        appUser.setActive(true);
        return appUser;
    }
}
