package com.donna.DTOs;

import com.donna.utils.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInvitationDTO {
    String email;
    RoleName role;
}

