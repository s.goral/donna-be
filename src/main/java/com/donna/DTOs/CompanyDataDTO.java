package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDataDTO {
    String name;
    int openingHour;
    int closingHour;
    List<String> services;
}
