package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class OnboardFormDTO {
    @Size(max = 256)
    String username;
    @Size(max = 256)
    String password;
    @Size(max = 256)
    String name;
    @Size(max = 256)
    String lastName;
    String token;
}
