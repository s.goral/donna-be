package com.donna.DTOs;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class LoginFormDTO {
    @Size(max = 256)
    String username;
    @Size(max = 256)
    String password;
}
