package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsDTO {
    Integer totalReservations;
    Integer canceledReservations;
    Integer newReservations;
    Integer finishedReservations;
}

