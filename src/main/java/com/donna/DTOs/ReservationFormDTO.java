package com.donna.DTOs;

import lombok.Data;

@Data
public class ReservationFormDTO {
    long employeeId;
    long clientId;
    String startDate;
    String endDate;
    String service;
}
