package com.donna.DTOs;

import com.donna.utils.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientFormDTO {
    Gender gender;
    String name;
    String lastName;
    LocalDate birthday;
    String phoneNumber;
    String email;
}

