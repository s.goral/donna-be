package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
public class RegisterFormDTO {
    @Size(max = 256)
    String username;
    @Size(max = 256)
    String password;
    @Size(max = 256)
    String companyName;
    @Size(max = 256)
    String name;
    @Size(max = 256)
    String lastName;
    int openingHour;
    int closingHour;
    List<String> services;
    String token;
}
