package com.donna.DTOs;

import com.donna.utils.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {
    Long id;
    String name;
    String lastName;
    String phoneNumber;
    String email;
    LocalDate joinDate;
    LocalDate birthday;
    Gender gender;
}

