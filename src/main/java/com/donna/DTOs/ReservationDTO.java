package com.donna.DTOs;

import com.donna.types.ReservationStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ReservationDTO {
    long id;
    long clientId;
    String clientName;
    LocalDateTime startDate;
    LocalDateTime endDate;
    String ownerName;
    long ownerId;
    ReservationStatus status;
    private String service;
}
