package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDataDTO {
    String name;
    String lastName;
    List<ReservationDTO> reservations;
}
