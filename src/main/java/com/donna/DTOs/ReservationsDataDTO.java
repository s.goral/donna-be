package com.donna.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ReservationsDataDTO {
    List<ReservationDTO> reservations;
    int openingHour;
    int closingHour;
    List<String> services;
}
