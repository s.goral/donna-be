package com.donna.DTOs;

import com.donna.utils.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    Long id;
    String name;
    String lastName;
    String email;
    RoleName roleName;
    boolean isActive;
}

