package com.donna.dao;

import com.donna.email.EmailRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRequestRepository extends CrudRepository<EmailRequest, Integer> {
    List<EmailRequest> findByRealized(boolean realized);

}
