package com.donna.dao;

import com.donna.entity.AppUser;
import com.donna.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppUserDAO extends PagingAndSortingRepository<AppUser, Long> {
    Optional<AppUser> findByUsername(String username);

    Boolean existsByUsername(String username);

    Optional<AppUser> findByEmployee(Employee employee);

    List<AppUser> findAll();

}
