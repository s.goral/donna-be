package com.donna.dao;

import com.donna.entity.Company;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyDAO extends PagingAndSortingRepository<Company, Long> {
    Optional<Company> findByName(String name);


    List<Company> findAll();
}
