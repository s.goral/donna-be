package com.donna.dao;

import com.donna.entity.Client;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientDAO extends PagingAndSortingRepository<Client, Long> {

    List<Client> findAll();
}
