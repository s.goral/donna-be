package com.donna.dao;

import com.donna.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeDAO extends PagingAndSortingRepository<Employee, Long> {

    List<Employee> findAll();
}
