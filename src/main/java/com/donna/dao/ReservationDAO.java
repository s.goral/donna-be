package com.donna.dao;

import com.donna.entity.Company;
import com.donna.entity.Reservation;
import com.donna.types.ReservationStatus;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationDAO extends PagingAndSortingRepository<Reservation, Long> {

    List<Reservation> findAll();

    List<Reservation> findAllByCompany(Company company);

    List<Reservation> findAllByStatusAndEndDateBefore(ReservationStatus status, LocalDateTime endDate);
}
